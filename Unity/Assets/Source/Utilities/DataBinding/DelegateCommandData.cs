﻿namespace de.FF.Utilities.DataBinding
{
    public class DelegateCommandData
    {
        public ICommand Command { get; private set; }


        public DelegateCommandData(ICommand command)
        {
            Command = command;
        }
    }

    public class DelegateCommandData<T>
    {
        public ICommand<T> Command { get; private set; }

        public T ExecuteParam;
        public T CanExecuteParam;


        public DelegateCommandData(ICommand<T> command, T executeParam, T canExecuteParam)
        {
            Command = command;
            ExecuteParam = executeParam;
            CanExecuteParam = canExecuteParam;
        }
    }
}