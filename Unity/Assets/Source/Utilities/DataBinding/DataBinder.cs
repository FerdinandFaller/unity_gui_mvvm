﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Object = System.Object;

namespace de.FF.Utilities.DataBinding
{
    public struct DataBindEndPoint
    {
        public object EndPoint;
        public string EndPointMemberName;
        public bool ImplementsINotifyPropertyChanged;

        public DataBindEndPoint(object endpoint, string memberName)
        {
            EndPoint = endpoint;
            ImplementsINotifyPropertyChanged =
                EndPoint.GetType().GetInterfaces().Contains(typeof (INotifyPropertyChanged));
            EndPointMemberName = memberName;
        }
    }

    public struct DataBindConnection
    {
        public DataBindEndPoint EndPointA;
        public DataBindEndPoint EndPointB;
    }

    public static class DataBinder
    {
        private static readonly List<DataBindConnection> Connections = new List<DataBindConnection>();

        /// <summary>
        /// Binds the given EndPoints. Calls OnPropertyChanged of Endpoint "a" after the connection is established.
        /// If both Endpoints implement INotifyPropertyChanged, its a two way binding.
        /// If either of the EndPointMemberNames is null or empty, the EndPointMemberName of the other EndPoint is being used.
        /// Call is ignored if a connection with the same Endpoints is already bound.
        /// Call is ignored if neither of the Endpoints implements INotifyPropertyChanged.
        /// Call is ignored if both Endpoint's EndPointMemberNames are null or empty.
        /// </summary>
        /// <param name="a">Source Endpoint</param>
        /// <param name="b">Target Endpoint</param>
        public static void Bind(DataBindEndPoint a, DataBindEndPoint b)
        {
            if (Connections.Contains(new DataBindConnection { EndPointA = a, EndPointB = b })) return;
            if (Connections.Contains(new DataBindConnection { EndPointA = b, EndPointB = a })) return;

            if (!a.ImplementsINotifyPropertyChanged && !b.ImplementsINotifyPropertyChanged) return;

            if (string.IsNullOrEmpty(a.EndPointMemberName) && string.IsNullOrEmpty(b.EndPointMemberName)) return;

            if (string.IsNullOrEmpty(a.EndPointMemberName))
            {
                a.EndPointMemberName = b.EndPointMemberName;
            }
            else if (string.IsNullOrEmpty(b.EndPointMemberName))
            {
                b.EndPointMemberName = a.EndPointMemberName;
            }

            Connections.Add(new DataBindConnection{ EndPointA = a, EndPointB = b });

            if (a.ImplementsINotifyPropertyChanged)
            {
                ((INotifyPropertyChanged)a.EndPoint).PropertyChanged -= OnPropertyChanged;
                ((INotifyPropertyChanged)a.EndPoint).PropertyChanged += OnPropertyChanged;
                OnPropertyChanged(a.EndPoint, new PropertyChangedEventArgs(a.EndPointMemberName));
            }

            if (b.ImplementsINotifyPropertyChanged)
            {
                ((INotifyPropertyChanged)b.EndPoint).PropertyChanged -= OnPropertyChanged;
                ((INotifyPropertyChanged)b.EndPoint).PropertyChanged += OnPropertyChanged;
                //OnPropertyChanged(b.EndPoint, new PropertyChangedEventArgs(b.EndPointMemberName));
            }
        }

        public static void UnBind(DataBindEndPoint a, DataBindEndPoint b)
        {
            var connectionA = new DataBindConnection { EndPointA = a, EndPointB = b };
            var connectionB = new DataBindConnection { EndPointA = b, EndPointB = a };
            var connectionAIsTrue = Connections.Contains(connectionA);
            var connectionBIsTrue = Connections.Contains(connectionB);
            if (!connectionAIsTrue && !connectionBIsTrue) return;

            var connection = connectionAIsTrue ? connectionA : connectionB;

            if (a.ImplementsINotifyPropertyChanged)
            {
                ((INotifyPropertyChanged)a.EndPoint).PropertyChanged -= OnPropertyChanged;
            }
            if (b.ImplementsINotifyPropertyChanged)
            {
                ((INotifyPropertyChanged)b.EndPoint).PropertyChanged -= OnPropertyChanged;
            }

            Connections.Remove(connection);
        }

        private static void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            foreach (var connection in Connections)
            {
                Object value;
                if (sender == connection.EndPointA.EndPoint && 
                    connection.EndPointA.EndPointMemberName == propertyChangedEventArgs.PropertyName)
                {
                    value = GetValue(connection.EndPointA.EndPoint, connection.EndPointA.EndPointMemberName);
                    SetValue(connection.EndPointB.EndPoint, connection.EndPointB.EndPointMemberName, value);
                }
                else if (   sender == connection.EndPointB.EndPoint && 
                            connection.EndPointB.EndPointMemberName == propertyChangedEventArgs.PropertyName)
                {
                    value = GetValue(connection.EndPointB.EndPoint, connection.EndPointB.EndPointMemberName);
                    SetValue(connection.EndPointA.EndPoint, connection.EndPointA.EndPointMemberName, value);
                }
            }
        }

        private static object GetValue(object obj, String memberName)
        {
            var member = obj.GetType().GetMember(memberName).Single();
            if (member == null) return null;

            switch (member.MemberType)
            {
                case MemberTypes.Property:
                    return ((PropertyInfo)member).GetValue(obj, null);
                case MemberTypes.Field:
                    return ((FieldInfo)member).GetValue(obj);
                default:
                    throw new Exception("Bad member type: " + member.MemberType);
            }
        }

        private static void SetValue(object obj, String memberName, object value)
        {
            var member = obj.GetType().GetMember(memberName).Single();

            if (member == null) return;

            switch (member.MemberType)
            {
                case MemberTypes.Property:
                    ((PropertyInfo)member).SetValue(obj, Convert.ChangeType(value, obj.GetType().GetProperty(memberName).PropertyType), null);
                    break;
                case MemberTypes.Field:
                    ((FieldInfo)member).SetValue(obj, Convert.ChangeType(value, obj.GetType().GetField(memberName).FieldType));
                    break;
                default:
                    throw new Exception("Bad member type: " + member.MemberType);
            }
        }
    }
}