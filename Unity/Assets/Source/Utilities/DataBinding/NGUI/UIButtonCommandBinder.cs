﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace de.FF.Utilities.DataBinding.NGUI
{
    public static class UIButtonCommandBinder
    {
        private static readonly Dictionary<UIButton, List<DelegateCommandData>> ButtonCommands = new Dictionary<UIButton, List<DelegateCommandData>>();
        private static readonly Dictionary<UIButton, List<DelegateCommandData<object>>> ButtonCommandsWithParams = new Dictionary<UIButton, List<DelegateCommandData<object>>>();


        public static void RaiseButtonClicked(UIButton button)
        {
            if (ButtonCommands.ContainsKey(button))
            {
                foreach (var delegateCommandData in ButtonCommands[button])
                {
                    delegateCommandData.Command.Execute();
                }
            }

            if (ButtonCommandsWithParams.ContainsKey(button))
            {
                foreach (var delegateCommandData in ButtonCommandsWithParams[button])
                {
                    delegateCommandData.Command.Execute(delegateCommandData.ExecuteParam);
                }
            }
        }

        public static void Bind(UIButton button, ICommand command)
        {
            if (command == null) throw new ArgumentNullException("command");

            var notifier = button.GetComponent<UIButtonOnClickBinderNotifier>();
            if (notifier == null)
            {
                button.gameObject.AddComponent<UIButtonOnClickBinderNotifier>();
            }

            if (!ButtonCommands.ContainsKey(button))
            {
                var dataList = new List<DelegateCommandData> { new DelegateCommandData(command) };

                ButtonCommands.Add(button, dataList);

                command.CanExecuteChanged += OnCommandCanExecuteChanged;
                command.RaiseCanExecuteChanged();
            }
            else
            {
                if (ButtonCommands[button].Any(delegateCommandData => delegateCommandData.Command == command))
                {
                    return;
                }

                ButtonCommands[button].Add(new DelegateCommandData(command));

                command.CanExecuteChanged += OnCommandCanExecuteChanged;
                command.RaiseCanExecuteChanged();
            }
        }

        public static void Bind(UIButton button, ICommand<object> command, object executeParameter, object canExecuteParameter)
        {
            if (command == null) throw new ArgumentNullException("command");

            var notifier = button.GetComponent<UIButtonOnClickBinderNotifier>();
            if (notifier == null)
            {
                button.gameObject.AddComponent<UIButtonOnClickBinderNotifier>();
            }

            if (!ButtonCommandsWithParams.ContainsKey(button))
            {
                var dataList = new List<DelegateCommandData<object>> {new DelegateCommandData<object>(command, executeParameter, canExecuteParameter)};

                ButtonCommandsWithParams.Add(button, dataList);

                command.CanExecuteChanged += OnCommandCanExecuteChangedWithParams;
                command.RaiseCanExecuteChanged();
            }
            else
            {
                if (ButtonCommandsWithParams[button].Any(delegateCommandData => delegateCommandData.Command == command &&
                                                                                Equals(delegateCommandData.ExecuteParam, executeParameter) &&
                                                                                Equals(delegateCommandData.CanExecuteParam, canExecuteParameter)))
                {
                    return;
                }

                ButtonCommandsWithParams[button].Add(new DelegateCommandData<object>(command, executeParameter, canExecuteParameter));

                command.CanExecuteChanged += OnCommandCanExecuteChangedWithParams;
                command.RaiseCanExecuteChanged();
            }
        }

        public static void UnBind(UIButton button, ICommand command)
        {
            if (command == null) throw new ArgumentNullException("command");

            if (ButtonCommands.ContainsKey(button))
            {
                var commandData = new DelegateCommandData(command);
                if (ButtonCommands[button].Contains(commandData))
                {
                    ButtonCommands[button].Remove(commandData);
                    command.CanExecuteChanged -= OnCommandCanExecuteChanged;
                }
            }
        }

        public static void UnBind(UIButton button, ICommand<object> command, object executeParameter, object canExecuteParameter)
        {
            if (command == null) throw new ArgumentNullException("command");

            if (ButtonCommandsWithParams.ContainsKey(button))
            {
                var commandData = new DelegateCommandData<object>(command, executeParameter, canExecuteParameter);
                if (ButtonCommandsWithParams[button].Contains(commandData))
                {
                    ButtonCommandsWithParams[button].Remove(commandData);
                    command.CanExecuteChanged -= OnCommandCanExecuteChangedWithParams;
                }
            }
        }

        private static void OnCommandCanExecuteChanged(object sender, EventArgs args)
        {
            // Does any of the CanExecute calls return false?
            foreach (var buttonCommand in ButtonCommands)
            {
                buttonCommand.Key.isEnabled = true;

                if (buttonCommand.Value.Any(commandData => !commandData.Command.CanExecute()))
                {
                    buttonCommand.Key.isEnabled = false;
                    return;
                }
            }
        }

        private static void OnCommandCanExecuteChangedWithParams(object sender, EventArgs args)
        {
            // Does any of the CanExecute calls return false?
            foreach (var buttonCommand in ButtonCommandsWithParams)
            {
                buttonCommand.Key.isEnabled = true;

                if (buttonCommand.Value.Any(commandData => !commandData.Command.CanExecute(commandData.CanExecuteParam)))
                {
                    buttonCommand.Key.isEnabled = false;
                    return;
                }
            }
        }
    }
}