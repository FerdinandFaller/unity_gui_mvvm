﻿using System.ComponentModel;
using UnityEngine;

namespace de.FF.Utilities.DataBinding.NGUI
{
    [RequireComponent(typeof(UILabel))]
    public class UILabelDataBinding : MonoBehaviour, INotifyPropertyChanged
    {
        private string _lastText;
        private UILabel _label;

        public string Text
        {
            get { return _label.text; }
            set
            {
                if (_label.text == value) return;

                _label.text = value;
                OnPropertyChanged("Text");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        void Awake()
        {
            _label = GetComponent<UILabel>();

            _lastText = Text;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        void Update()
        {
            if (string.CompareOrdinal(Text, _lastText) == 0) return;

            OnPropertyChanged("Text");

            _lastText = Text;
        }
    }
}