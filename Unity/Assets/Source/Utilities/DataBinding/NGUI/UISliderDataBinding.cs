﻿using System;
using System.ComponentModel;
using UnityEngine;

namespace de.FF.Utilities.DataBinding.NGUI
{
    [RequireComponent(typeof(UISlider))]
    public class UISliderDataBinding : MonoBehaviour, INotifyPropertyChanged
    {
        private UISlider _slider;

        public float Value
        {
            get { return _slider.value; }
            set
            {
                if (Math.Abs(_slider.value - value) < 0.001f) return;

                _slider.value = value;
                RaisePropertyChanged("Value");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        void Awake()
        {
            _slider = GetComponent<UISlider>();

            _slider.onChange.Add(new EventDelegate(OnSliderValueChanged));
        }

        private void OnSliderValueChanged()
        {
            RaisePropertyChanged("Value");
        }

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}