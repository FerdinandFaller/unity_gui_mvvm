﻿using UnityEngine;

namespace de.FF.Utilities.DataBinding.NGUI
{
    [RequireComponent(typeof(UIButton))]
    public class UIButtonOnClickBinderNotifier : MonoBehaviour
    {
        protected UIButton Button;


        protected virtual void Awake()
        {
            Button = GetComponent<UIButton>();
            Button.onClick.Add(new EventDelegate(OnButtonClick));
        }

        private void OnButtonClick()
        {
            UIButtonCommandBinder.RaiseButtonClicked(Button);
        }
    }
}