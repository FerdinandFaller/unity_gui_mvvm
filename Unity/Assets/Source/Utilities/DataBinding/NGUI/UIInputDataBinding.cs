﻿using System.ComponentModel;
using UnityEngine;

namespace de.FF.Utilities.DataBinding.NGUI
{
    [RequireComponent(typeof(UIInput))]
    public class UIInputDataBinding : MonoBehaviour, INotifyPropertyChanged
    {
        private UIInput _input;

        public string Value
        {
            get { return _input.value; }
            set
            {
                if (_input.value == value) return;

                _input.value = value;
                OnPropertyChanged("Value");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        void Awake()
        {
            _input = GetComponent<UIInput>();

            _input.onChange.Add(new EventDelegate(OnInputChanged));
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        void OnInputChanged()
        {
            OnPropertyChanged("Value");
        }
    }
}