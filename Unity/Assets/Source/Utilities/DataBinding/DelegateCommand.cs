﻿using System;

namespace de.FF.Utilities.DataBinding
{
    public class DelegateCommand<T> : DelegateCommandBase<T>
    {
        public DelegateCommand(Action<T> executeMethod)
            : base(o => executeMethod(o))
        {

        }

        public DelegateCommand(Action<T> executeMethod,Func<T,bool> canExecuteMethod ): base(o => executeMethod(o), (o) => canExecuteMethod(o))
        {
        
        }
    }

    public class DelegateCommand : DelegateCommandBase
    {
        public DelegateCommand(Action executeMethod) : base(executeMethod)
        {

        }

        public DelegateCommand(Action executeMethod, Func<bool> canExecuteMethod)
            : base(executeMethod, canExecuteMethod)
        {

        }
    }
}