﻿using System;

namespace de.FF.Utilities.DataBinding
{
    public class DelegateCommandBase : ICommand
    {
        private readonly Func<bool> _canExecute;
        private readonly Action _execute;

        public event EventHandler CanExecuteChanged;


        public DelegateCommandBase(Action excute)
            : this(excute, null)
        {
            
        }

        public DelegateCommandBase(Action execute, Func<bool> canExecute)
        {
            _canExecute = canExecute;
            _execute = execute;
        }

        public bool CanExecute()
        {
            return _canExecute == null || _canExecute();
        }

        public void Execute()
        {
            if (CanExecute())
            {
                _execute();
            }
        }

        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, EventArgs.Empty);
            }
        }
    }

    public class DelegateCommandBase<T> : ICommand<T>
    {
        private readonly Predicate<T> _canExecute;
        private readonly Action<T> _execute;

        public event EventHandler CanExecuteChanged;


        public DelegateCommandBase(Action<T> excute)
            : this(excute, null)
        {

        }

        public DelegateCommandBase(Action<T> execute, Predicate<T> canExecute)
        {
            _canExecute = canExecute;
            _execute = execute;
        }

        public bool CanExecute(T param)
        {
            return _canExecute == null || _canExecute(param);
        }

        public void Execute(T param)
        {
            if (CanExecute(param))
            {
                _execute(param);
            }
        }

        public void RaiseCanExecuteChanged()
        {
            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, EventArgs.Empty);
            }
        }
    }
}