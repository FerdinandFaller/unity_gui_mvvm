﻿using System;

namespace de.FF.Utilities.DataBinding
{
    public interface ICommand<in T>
    {
        event EventHandler CanExecuteChanged;

        bool CanExecute(T param);

        void Execute(T param);

        void RaiseCanExecuteChanged();
    }

    public interface ICommand
    {
        event EventHandler CanExecuteChanged;

        bool CanExecute();

        void Execute();

        void RaiseCanExecuteChanged();
    }
}