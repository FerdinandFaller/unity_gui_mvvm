using System;
using UnityEngine;

namespace de.FF.GUI
{
    public abstract class View<T> : MonoBehaviour where T : ViewModel
    {
        protected T ViewModel;
		

        protected virtual void Awake()
        {
            DontDestroyOnLoad(this);
            ViewModel = FindObjectOfType(typeof(T)) as T;
            if(ViewModel == null) Debug.LogError("ViewModel == null", this);

            ActivateDeactivate(false);

            transform.localPosition = Vector3.zero;

            if (ViewModel != null)
            {
                ViewModel.ActiveViewAddedEvent -= OnViewModelActiveViewAdded;
                ViewModel.ActiveViewAddedEvent += OnViewModelActiveViewAdded;
            }
        }

        protected abstract void ActivateDeactivate(bool shouldBeActive);

        protected abstract void OnViewModelActiveViewAdded(Type type);
    }
}