using System;
using UnityEngine;

namespace de.FF.GUI
{
    [RequireComponent(typeof(UIPanel))]
    abstract class ViewNgui<T> : View<T> where T : ViewModel
    {
        protected UIPanel Panel;


        protected override void Awake()
        {
            Panel = gameObject.GetComponent<UIPanel>();

            base.Awake();
        }

        protected override void OnViewModelActiveViewAdded(Type type)
        {
            ActivateDeactivate(GetType() == type);
        }

        protected override void ActivateDeactivate(bool shouldBeActive)
        {
            Panel.alpha = shouldBeActive ? 1f : 0f;
        }
    }
}