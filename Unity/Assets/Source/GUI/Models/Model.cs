using System.ComponentModel;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace de.FF.GUI
{
    public abstract class Model : MonoBehaviour, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public static Model Instance { get; private set; }

        protected List<Type> ActiveViews;
        public event Action<Type> ActiveViewAddedEvent;
        public event Action<Type> ActiveViewRemovedEvent;


        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        void Awake()
        {
            Instance = this;

            DontDestroyOnLoad(this);

            if (ActiveViews == null) ActiveViews = new List<Type>();
        }

        public void AddActiveView(Type type)
        {
            if (ActiveViews == null) ActiveViews = new List<Type>();
            if (ActiveViews.Contains(type)) return;
            if (!type.IsClass || type.IsAbstract || !type.IsSubclassOf(typeof (View<ViewModel>))) return;

            ActiveViews.Add(type);

            if (ActiveViewAddedEvent != null)
            {
                ActiveViewAddedEvent(type);
            }
        }

        public void RemoveActiveView(Type type)
        {
            if (ActiveViews == null) ActiveViews = new List<Type>();
            if (!ActiveViews.Contains(type)) return;
            if (!type.IsClass || type.IsAbstract || !type.IsSubclassOf(typeof(View<ViewModel>))) return;

            ActiveViews.Remove(type);

            if (ActiveViewRemovedEvent != null)
            {
                Debug.Log("Removing Active View: " + type);
                ActiveViewRemovedEvent(type);
            }
        }

        public void RemoveAllActiveView()
        {
            foreach (var activeViewType in ActiveViews)
            {
                if (ActiveViewRemovedEvent != null)
                {
                    ActiveViewRemovedEvent(activeViewType);
                }
            }

            ActiveViews.Clear();
        }

        public bool IsViewActive(Type type)
        {
            return ActiveViews.Contains(type);
        }
    }
}