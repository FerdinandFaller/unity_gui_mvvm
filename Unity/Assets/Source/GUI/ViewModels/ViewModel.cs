﻿using System;
using de.FF.Utilities.DataBinding;
using System.ComponentModel;
using UnityEngine;

namespace de.FF.GUI
{
    public abstract class ViewModel : MonoBehaviour, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand<object> AddActiveViewCommand { get; set; }
        public ICommand<object> RemoveActiveViewCommand { get; set; }

        public event Action<Type> ActiveViewAddedEvent;


        protected virtual void RaisePropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void Awake()
        {
            DontDestroyOnLoad(this);

            Model.Instance.ActiveViewAddedEvent -= OnModelActiveViewAdded;
            Model.Instance.ActiveViewAddedEvent += OnModelActiveViewAdded;

            if (AddActiveViewCommand == null)
            {
                AddActiveViewCommand = new DelegateCommand<object>(ExecuteAddActiveView);
            }

            if (RemoveActiveViewCommand == null)
            {
                RemoveActiveViewCommand = new DelegateCommand<object>(ExecuteRemoveActiveView);
            }
        }

        private void ExecuteAddActiveView(object obj)
        {
            AddActiveView(obj.GetType());
        }

        private void ExecuteRemoveActiveView(object obj)
        {
            RemoveActiveView(obj.GetType());
        }

        protected void AddActiveView(Type type)
        {
            Model.Instance.AddActiveView(type);
        }

        protected void RemoveActiveView(Type type)
        {
            Model.Instance.RemoveActiveView(type);
        }

        public bool IsViewActive(Type type)
        {
            return Model.Instance.IsViewActive(type);
        }

        protected void OnModelActiveViewAdded(Type type)
        {
            if (ActiveViewAddedEvent != null)
            {
                ActiveViewAddedEvent(type);
            }
        }
    }
}